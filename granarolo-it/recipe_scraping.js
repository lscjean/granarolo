/** DataLayer information pushed via this Code 
 * 
 * tealium_event = 'recipe_view' ok
 * recipe_name = // dynamic ok
 * recipe_image // url ok
 * recipe_category // ok
 * recipe_details {skills,duration,healthiness,servings} // ok
 * recipe_collection {name, category, img, url, details} // ok
 * product_name // ok
 * product_brand // ok
 * product_category // ok
 * product_img // url ok
 * product_url // url ok
 *  
*/


var breadcrumbs = Array.prototype.slice.call(document.getElementById("breadcrumb").children).map( function(x){return x.innerText} ).map( function(x){return x.trim()} );
var ingredient_title = document.getElementsByClassName('recipes__ingredients__title ng-binding')[0].innerText;
var domain = document.location.protocol+'//'+document.location.hostname;
var url = domain + document.location.pathname;
var product_urlSplit = $('.recipes__related-product__text').children('a').attr('href').split('/');

if (breadcrumbs[1] == "Ricette" && document.getElementById('recipes') !== null){

    var related_product_url = document.querySelector('#recipes.container-fluid div.recipes__tips-mobile-wrapper div.recipes__related-product div.recipes__related-product__text a').href
    var recipe_page_details = document.getElementsByClassName("recipes__info__icon__title ng-binding")
    var recipe_info = $('.recipes__info__icon__title').map(function(){return $(this).text()}).get()
    
    /** Page information backup */   
    utag.data.tealium_event = 'recipe_view';
    utag.data.website_section = 'recipes';
    utag.data.page_category = 'recipes_list';
    utag.data.page_section = 'recipe_page';

    /** Recipe Page information */
    utag.data.recipe_name = $('.recipes__info__title').text()
    utag.data.recipe_img = domain + $('.recipes__cover__image__print').attr('src');
    utag.data.recipe_category = recipe_info[0];
    utag.data.recipe_url = url;

    utag.data.product_name = $('.recipes__related-product__subtitle').text();
    utag.data.product_brand = product_urlSplit[2];
    utag.data.product_category = product_urlSplit[3];
    utag.data.product_url = domain + $('.recipes__related-product__text').children('a').attr('href');
    utag.data.product_img = domain + $('a.recipes__related-product__image-wrapper').attr('href');

    
    /** decide to keep them seperate of the recipe_details object 
    utag.data.recipe_skills = '';
    utag.data.recipe_duration = '';
    utag.data.recipe_heathiness = '';
    utag.data.recipe_serving = '';
    */
    
    /** Recipe Details Function */
        function recipeDetails(skills, duration, healthiness, serving){
            this.skills = skills;
            this.duration = duration;
            this.healthiness = healthiness;
            this.serving = serving;
        }

        utag.data.recipe_details = new recipeDetails(
            recipe_info[1],
            recipe_info[2],
            recipe_info[4],
            recipe_info[5]
        )


    /** Recipe Collection Function */
        function recipeCollection(name, category, img, url, details){
            this.name = name;
            this.category = category;
            this.img = img;
            this.url = url;
            this.details = details;
        }
        
        utag.data.recipe_collection = new recipeCollection(
            utag.data.recipe_name,
            utag.data.recipe_category,
            utag.data.recipe_img,
            utag.data.recipe_url,
            utag.data.recipe_details
        )
        
/** Product Collection Function  **/
    function productDetails(id, name, brand, category, img, url){
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.category = category;
        this.img = img;
        this.url = url;
    }
    
    utag.data.product_collection = new productDetails(
        utag.data.product_id,
        utag.data.product_name,
        utag.data.product_brand, //product_brand
        utag.data.product_category, //product_category
        utag.data.product_img,
        url,
    )
    
    
    /**     "tealium_event": "recipe_page_information",
            "product_name" : related_product_url.split('/')[6],
            "product_brand" : related_product_url.split('/')[4],
            "product_category": related_product_url.split('/')[5],
            "recipe_type" : recipe_page_details[0].innerText,
            "recipe_difficulty" : recipe_page_details[1].innerText,
            "recipe_duration" : recipe_page_details[2].innerText,
            "recipe_preparation" : recipe_page_details[3].innerText,
            "recipe_healthiness" : recipe_page_details[4].innerText,
            "recipe_quantity" : recipe_page_details[5].innerText,
            "recipe_category" : document.getElementsByClassName('recipes__related-recipes__title')[0].innerText,
            "test" : "helloooo"
        });
     */
    console.log('recipe information has loaded successfully');
    console.log(utag.data.recipe_collection);
    console.log(utag.data.product_collection)
} 

