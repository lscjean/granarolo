var RelatedProductName = $('.product__related .browse-grid-box').map(function(){return $(this).text().trim();}).get();
var RelatedProductURL = $('.related-container').find('a').map(function(){ return document.location.hostname + $(this).attr('href')}).get();
var RelatedProductIMG = $('.product__related .browse-grid-box').map( function() {return document.location.hostname + $(this).html().split('url(')[1].split(')')[0];}).get();
var RelatedProductBrand = $('.related-container').find('a').map(function(){ return $(this).attr('href').split('/')[2]}).get();
var RelatedProductCategory = $('.related-container').find('a').map(function(){ return $(this).attr('href').split('/')[3]}).get();

function relatedProduct(name, image, link, brand, category){
    this.name = name;
    this.image = image;
    this.link = link;
    this.link = brand;
    this.link = category;
}

	var relatedProduct_Collection = []
for (var i = 0; i <= RelatedProductName.length-1 ; i++){
    product = new relatedProduct(
        RelatedProductName[i],
        RelatedProductIMG[i],
        RelatedProductURL[i],
        RelatedProductBrand[i],
        RelatedProductCategory[i]
    )
	relatedProduct_Collection.push(product)
}