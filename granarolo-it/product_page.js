/**
 * product id
 * product name
 * product legal
 * product brand
 * product category
 * product image
 * product ingredient
 * product nutriment
 * product collection
 * 
 * related product collection
 * related Recipe Collection
 * 
 */

if($('.product__formats-ean__item__code').length !== 0){

    /** custom event **/
    utag.data.tealium_event = 'product_page';

    /** page_info **/
    var prot = document.location.protocol+'//';
    var domain = prot + document.location.hostname;
    var pathname = document.location.pathname;
    var pathname_parsener = pathname.split('/')
    
    
    /** Product Information **/
    utag.data.product_id = document.querySelector('.product__formats-ean__item__code').innerText;
    utag.data.product_name =  $('.product__main-title').text().trim();
    utag.data.product_legal = $('.product__main-legal').text().split(':')[1].trim()
    utag.data.product_brand = pathname_parsener[2] ;
    utag.data.product_category = pathname_parsener[3] ;

    if ($('.product__main-image').attr('src') !== undefined)
            {utag.data.product_img = domain + $('.product__main-image').attr('src');}
    else    {utag.data.product_img = domain + $('img.product__slide__image:eq(1)').attr('src');}        
    
        /** Product Attributes **/
            utag.data.product_ingredients = document.querySelector('.product__consumer-info__item__description').innerText.toLowerCase().trim();
            utag.data.product_nutriment = 'tbd';
            
    /** Product Collection Function  **/
    function productDetails(id, name, brand, category, img, url){
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.category = category;
        this.img = img;
        this.url = url;
    }
    
    utag.data.product_collection = new productDetails(
        utag.data.product_id,
        $('.product__main-legal').text().split(':')[1].trim(), //product_legal_name
        utag.data.product_brand, //product_brand
        utag.data.product_category, //product_category
        utag.data.product_img,
        domain + pathname,
    )

    /** Related Collection Function */
    var RelatedProductName = $('.product__related .browse-grid-box').map(function(){return $(this).text().trim();}).get();
    var RelatedProductURL = $('.related-container').find('a').map(function(){ return document.location.hostname + $(this).attr('href')}).get();
    var RelatedProductIMG = $('.product__related .browse-grid-box').map( function() {return document.location.hostname + $(this).html().split('url(')[1].split(')')[0];}).get();
    var RelatedProductBrand = $('.related-container').find('a').map(function(){ return $(this).attr('href').split('/')[2]}).get();
    var RelatedProductCategory = $('.related-container').find('a').map(function(){ return $(this).attr('href').split('/')[3]}).get();

    function relatedProduct(name, image, link, brand, category){
        this.name = name;
        this.image = image;
        this.link = link;
        this.brand = brand;
        this.category = category;
    }

        utag.data.relatedProduct_collection = []
    for (var i = 0; i <= RelatedProductName.length-1 ; i++){
        product = new relatedProduct(
            RelatedProductName[i],
            RelatedProductIMG[i],
            RelatedProductURL[i],
            RelatedProductBrand[i],
            RelatedProductCategory[i]
        )
        utag.data.relatedProduct_collection.push(product)
    }

    var relatedRecipeURL = $('a.product__related-recipe__cover').map(function(){return $(this).attr('href')}).get();
    var relatedRecipeNAME = $('a.product__related-recipe__cover').map(function(){return $(this).attr('title')}).get();
    var relatedRecipeIMG = $('a.product__related-recipe__cover').map(function(){return $(this).css('background-image').replace('url(','').replace(')','').replace(/\"/gi, "")}).get();

    function relatedRecipe(name, image, url){
        this.name = name;
        this.image = image;
        this.url = url;
    }

        utag.data.relatedRecipe_collection = []
    for (var i = 0; i <= relatedRecipeNAME.length-1 ; i++){
        recipe = new relatedRecipe(
            relatedRecipeNAME[i],
            relatedRecipeIMG[i],
            relatedRecipeURL[i]
        )
        utag.data.relatedRecipe_collection.push(recipe)
    }

        console.log('product information are loaded ')
        console.log(utag.data.product_collection);
        console.log(utag.data.relatedRecipe_collection);
        console.log(utag.data.relatedProduct_collection);
}
    
