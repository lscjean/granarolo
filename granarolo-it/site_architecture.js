/* website architecture 

    homePage{             
        website_section = 'homepage',
        page_category = 'homepage',
        page_subcategory = 'homepage',
        page_type = 'homepage',
    } // ok

    Corporate{             
        website_section = 'corporate',
        page_category = 'about_us',
        page_subcategory = '',
        page_type = 'about_us',
    } // ok

    StoreLocator{             
        website_section = 'store_locator',
        page_category = 'store_locator',
        page_subcategory = '',
        page_type = 'store_locator',
    } // ok

    brand_catalogue{             
        website_section = 'catalogue',
        page_category = 'brands',
        page_subcategory = brand '- catalogue',
        page_type = 'brand_catalogue',
    } // ok

    product_catalogue{             
        website_section = 'catalogue',
        page_category = 'products',
        page_subcategory = productCategory '- catalogue',
        page_type = 'product_catalogue',
    } // ok

    product_page{             
        website_section = 'catalogue',
        page_category = 'brands',
        page_subcategory = '',
        page_type = 'product_page',
    } // ok

    recipe_page{             
        website_section = 'recipes',
        page_category = 'recipe_list',
        page_subcategory = '', || 'recipes_search',
        page_type = 'recipe_page',
    } // ok

*/
/** to capitalize function */
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

/* shortcuts */

var path = document.location.pathname;
var urlParams = new URLSearchParams(window.location.search);

/** page collection function */
function pageCollection(website_section, page_category, page_subcategory, page_type, page_name){
    this.section = website_section;
    this.category = page_category;
    this.subcategory = page_subcategory;
    this.type = page_type;
    this.name = page_name;
}

/* homepage */

    var path = document.location.pathname;
    var tl = utag.data;

    if (path === '/'){
        utag.data.website_section = 'homepage';
        utag.data.page_category = 'homepage';
        utag.data.page_subcategory = 'homepage';
        utag.data.page_type = 'homepage';
        }
    
/** Corporate Content Page */

    else if (path.includes('chi-siamo')){
        utag.data.website_section = 'corporate';
        utag.data.page_category = 'about_us';
        utag.data.page_subcategory = '';
        utag.data.page_type = 'about_us';
    }

/** Store Locator */

else if (path.includes('storelocator')){
    utag.data.website_section = 'store_locator';
    utag.data.page_category = 'store_locator';
    utag.data.page_subcategory = '';
    utag.data.page_type = 'store_locator';
}

/* brand catalogue */
    else if (path.includes('marchi') === true && document.querySelector('.product__formats-ean__item__code') === null ){
        utag.data.website_section = 'catalogue';
        utag.data.page_category = 'brands';
        utag.data.page_type = utag.data.page_category +'_'+ utag.data.website_section;
            if (utag.data.breadcrumbs[3] === undefined){
                utag.data.product_brand = path.split('/')[2];
                utag.data.page_subcategory = utag.data.breadcrumbs[2] + ' - all catalogue';
            }
            else if(utag.data.breadcrumbs[3].toLowerCase() == 'tutti i prodotti') {
                utag.data.product_brand = path.split('/')[2];
                utag.data.page_subcategory = utag.data.breadcrumbs[2] + ' - all catalogue';
            }    
            else if (utag.data.breadcrumbs[3] !== null){
                utag.data.product_brand = path.split('/')[2];
                utag.data.product_category = path.split('/')[3];
                utag.data.page_subcategory = utag.data.breadcrumbs[2] + ' - ' + utag.data.breadcrumbs[3];
            } 
    }


/* product catalogue */
    else if (path.includes('prodotti') === true){
        utag.data.website_section = 'catalogue';
        utag.data.page_category = 'products';
        utag.data.page_type = utag.data.page_category +'_'+ utag.data.website_section;
        utag.data.product_category = path.split('/')[2];
            if (utag.data.breadcrumbs[3] === undefined || utag.data.breadcrumbs[3].toLowerCase() === 'tutti'){
                utag.data.page_subcategory = utag.data.product_category.replace('-',' ').capitalize() + " - all catalogue" ;
            }
            else  if (utag.data.breadcrumbs[3] !== undefined){
                utag.data.page_subcategory = utag.data.product_category.replace('-',' ').capitalize() + ' - ' + utag.data.breadcrumbs[3];
            } 
    }


/* product Page */
    else if (document.querySelector('.product__formats-ean__item__code') !== null ){
        utag.data.website_section = 'catalogue';
        utag.data.page_category = 'brands';
        /* utag.data.page_subcategory = ''; */
        utag.data.page_type = 'product_page';
        utag.data.page_name = $('.product__main-title').text().trim();
    }


/* Recipe Page */

    else if (utag.data.breadcrumbs[2] !== undefined && document.querySelector('.recipes-body') !== null){
        utag.data.website_section = 'recipes';
        utag.data.page_category = 'recipes_list';
        utag.data.page_type = 'recipe_page';
        if (utag.data.page_referrer !== undefined && utag.data.page_referrer.includes('risultati-della-ricerca')){
            utag.data.page_subcategory = 'recipes_search';
        }
        else {utag.data.page_subcategory = ''}
    }

/* Recipes Catalogue */
else if (path.includes('ricette') === true && path.split('/')[2] !== 'risultati-della-ricerca'){
    utag.data.website_section = 'recipes';
    utag.data.page_category = 'recipes_list';
    utag.data.page_type = 'recipe_list';
}

else if (path.includes('ricette') === true && path.split('/')[2] == 'risultati-della-ricerca'){
    utag.data.website_section = 'recipes';
    utag.data.page_category = 'recipes_list';
    utag.data.page_subcategory = 'recipes_search';
    utag.data.page_type = 'recipes_search';
    utag.data.search_results = urlParams.get('recipe_tags');
    utag.data.search_term = urlParams.get('q');
    utag.data.search_type = 'recipe_search';
}

/** collect product info for readability */
utag.data.page_collection = new pageCollection(
    utag.data.website_section,
    utag.data.page_category,
    utag.data.page_subcategory,
    utag.data.page_type,
    utag.data.page_name
)

console.log('site architecture DL has loaded successfully')
console.log(utag.data.page_collection)
