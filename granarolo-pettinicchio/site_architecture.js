if (document.location.pathname !== undefined){

    var domain = document.location.protocol+'//'+document.location.hostname;
    var path = document.location.pathname;
    var url = domain + path;
    var b = utag.data;
    var path = document.location.pathname;
    var urlParams = new URLSearchParams(window.location.search);
    var hash = window.location.hash;
    var pathSplit = path.split('/') ;
    var pathLEN = path.split('/').length;
        b['page_name'] = document.title

    /** page collection function */

    function pageCollection(website_section, page_category, page_subcategory, page_type, page_name){
        this.section = website_section;
        this.category = page_category;
        this.subcategory = page_subcategory;
        this.type = page_type;
        this.name = page_name;
    }

    /* homepage */
        if (path === '/'){
            b['website_section'] = 'homepage';
            b['page_category'] = 'homepage';
            b['page_subcategory'] = 'homepage';
            b['page_type'] = 'homepage';
        }

    /** corporate content */
        else if (path.includes("/gusto-e-tradizione")){
            b['website_section'] = 'content';
            b['page_category'] = 'about-us';
            b['page_subcategory'] = '';
            b['page_type'] = 'content_page';
        }

    /** product catalogue */
        else if (path.includes('prodotti') && pathLEN === 2){
            b['website_section'] = 'catalogue';
            b['page_category'] = 'product';
            b['page_type'] = 'product_catalogue';
            b['page_subcategory'] = 'all';
        }

    /** product Page */
        else if (path.includes('prodotto') && pathLEN !== 2){
            b['website_section'] = 'catalogue';
            b['page_category'] = 'product';
            b['page_type'] = 'product_page';
            b['page_subcategory'] = pathSplit[2];
        }

    /** Recipe Page */
        else if (path.includes('ricetta') && jQuery('.recipes__info__icon').map(function(){return $(this).text().trim()}).get().length){
            b['website_section'] = 'recipes';
            b['page_category'] = 'recipes_page';
            b['page_type'] = 'recipe_page';
            b['page_subcategory'] = pathSplit[2];
        }

    /** Recipe Catalogue */
        else if (path.includes('ricette')){
            b['website_section'] = 'recipes';
            b['page_category'] = 'recipes_list';
            b['page_type'] = 'recipe_list';
            if (path.includes('risultati-della-ricerca')){
                b['page_subcategory'] = 'recipes_search';
                b['search_results'] = urlParams.get('recipe_tags');
                b['search_term'] = urlParams.get('q');
                b['search_type'] = 'recipe_search';
            }
            else {b['page_subcategory'] = 'all'}
        }

    /** Contact page */
        else if (path.includes('contatti')){
            b['website_section'] = 'corporate';
            b['page_category'] = 'contact_page';
            b['page_type'] = 'corporate_page';
            if (pathLEN !== 2){
                b['page_subcategory'] = pathSplit[2];
            }
            else {b['page_subcategory'] = 'general'}
        }

    /** collect product info for readability */
    b['page_collection'] = new pageCollection(
        b['website_section'],
        b['page_category'],
        b['page_subcategory'],
        b['page_type'],
        b['page_name']
    );
    console.log('site architecture DL has loaded successfully');
    console.log(b['page_collection']);
}
