if (document.location.pathname !== undefined){
    /* website architecture 
    
        homePage{             
            website_section = 'homepage',
            page_category = 'homepage',
            page_subcategory = 'homepage',
            page_type = 'homepage',
        } // ok
    
        product_catalogue{             
            website_section = 'catalogue',
            page_category = 'products',
            page_subcategory = productCategory '- catalogue',
            page_type = 'product_catalogue',
        } // ok
    
        product_page{             
            website_section = 'catalogue',
            page_category = 'brands',
            page_subcategory = '',
            page_type = 'product_page',
        } // ok
    
        recipe_page{             
            website_section = 'recipes',
            page_category = 'recipe_list',
            page_subcategory = '', || 'recipes_search',
            page_type = 'recipe_page',
        } // ok
    
    */
    /** to capitalize function */
    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    };
    
    /* shortcuts */
    
        // b['breadcrumb = jQuery('.breadcrumb').textContent.split('\n').map(function(x){return x.trim()}).slice(1);
    var domain = document.location.protocol+'//'+document.location.hostname;
    var path = document.location.pathname;
    var url = domain + path;
    var b = utag.data;
    var path = document.location.pathname;
    var urlParams = new URLSearchParams(window.location.search);
    var pathSplit = path.split('/') ;
    var pathLenght = path.split('/').length;
    
    /** page collection function */
    function pageCollection(website_section, page_category, page_subcategory, page_type, page_name){
        this.section = website_section;
        this.category = page_category;
        this.subcategory = page_subcategory;
        this.type = page_type;
        this.name = page_name;
    }
    
    /* homepage */
    
        var path = document.location.pathname;
    
        if (path === '/'){
            b['website_section'] = 'homepage';
            b['page_category'] = 'homepage';
            b['page_subcategory'] = 'homepage';
            b['page_type'] = 'homepage';
            }
    
    /* product Page */
    else if (jQuery('#modalita-conservazione > p:nth-child(10)').length === 1){
        b['website_section'] = 'catalogue';
        b['page_category'] = 'brands';
        b['page_subcategory'] = pathSplit[2];
        b['page_type'] = 'product_page';
        b['page_name'] = jQuery('.product-page-title').textContent;
    }
    
    /* product catalogue */
        else if (path.includes('prodotti') === true){
            b['website_section'] = 'catalogue';
            b['page_category'] = 'products';
            b['page_type'] = b['page_category'] +'_'+ b['website_section'];
            b['product_subcategory'] = pathSplit[2];
        }
    
    /* Recipe Page */
        else if(path.includes('ricette') === true && jQuery('.page-type-title').text() === 'Le Ricette'){
            b['website_section'] = 'recipes';
            b['page_category'] = 'recipes_page';
            b['page_type'] = 'recipe_page';
            b['page_subcategory'] = jQuery('.article-category').text().toLowerCase();
        }
    
    /* Recipes Catalogue */
    else if (path.includes('ricette') === true && jQuery('.posts-grid-reciepes').length){
        b['website_section'] = 'recipes';
        b['page_category'] = 'recipes_list';
        b['page_type'] = 'recipe_list';
        if (pathSplit.length === 2){
            b['page_subcategory'] = 'all';
        }
        else {
            b['page_subcategory'] = pathSplit[2];
        }
    }

    /* Content Page: NEWS */
    else if ( path.includes('notizie-fresche') && jQuery('.posts-grid').children().length === 0 ){
        b['website_section'] = 'news';
        b['page_category'] = 'news_page';
        b['page_subcategory'] = jQuery('.article-category').text().toLowerCase();
        b['page_type'] = 'content_page';
    }
    
    /* Content Catalogue: NEWS */
    else if ( path.includes('notizie-fresche') && jQuery('.posts-grid').children().length !== 0 ){
        b['website_section'] = 'news';
        b['page_category'] = 'news_list';
        b['page_type'] = 'content_list';
        if (pathLenght === 2){
           b['page_subcategory'] = 'all';
        }
        else if (pathLenght !== 2){
            b['page_subcategory'] = pathSplit[2];
         }   
    }
    
    /* Content Page: EXPERTS */
    else if (path.includes('/esperti/')){
        b['website_section'] = 'experts';
        if ((path.includes('psicologa') || path.includes('psicologo')) && (path === '/esperti/la-psicologa-comportamentale')) {
            b['page_category'] = 'tips_page';
            b['page_subcategory'] = 'psychology_presentation';
            }
        else if ((path.includes('psicologa') || path.includes('psicologo')) && (path.includes('/approfondimenti/') || path.includes('/consigli/'))){
            b['page_category'] = 'tips_page';
            b['page_subcategory'] = 'psychology_'+pathSplit[2];
            }
        else if ((path.includes('nutrizionista')) && (path === '/esperti/il-pediatra-nutrizionista')) {
            b['page_category'] = 'tips_page';
            b['page_subcategory'] = 'nutrition_presentation';
            }
        else if  ((path.includes('nutrizionista')) && (path.includes('approfondimenti') || path.includes('consigli'))) {
            b['page_category'] = 'tips_page';
            b['page_subcategory'] = 'nutrition_'+pathSplit[2];
        };
        b['page_type'] = 'content_page';
    }
    
    /** Corporate Content Page */
    
    else if(path.includes('/granarolo-bimbi/')){
        b['website_section'] = 'corporate';
        b['page_category'] = 'about_us';
        b['page_subcategory'] = pathSplit[2]
        b['page_type'] = 'corporate_page';
    };
    
    
    
    /** collect product info for readability */
    b['page_collection'] = new pageCollection(
        b['website_section'],
        b['page_category'],
        b['page_subcategory'],
        b['page_type'],
        b['page_name']
    );
    
    console.log('site architecture DL has loaded successfully');
    console.log(b['page_collection']);
}
    