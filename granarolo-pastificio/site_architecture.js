if (document.location.pathname !== undefined){

    var domain = document.location.protocol+'//'+document.location.hostname;
    var path = document.location.pathname;
    var url = domain + path;
    var b = utag.data;
    var path = document.location.pathname;
    var urlParams = new URLSearchParams(window.location.search);
    var hash = window.location.hash;
    var pathSplit = path.split('/') ;
    var pathLEN = path.split('/').length;
        b['page_name'] = document.title

    /** page collection function */

    function pageCollection(website_section, page_category, page_subcategory, page_type, page_name){
        this.section = website_section;
        this.category = page_category;
        this.subcategory = page_subcategory;
        this.type = page_type;
        this.name = page_name;
    }

    /* homepage */
        if (path === '/'){
            b['website_section'] = 'homepage';
            b['page_category'] = 'homepage';
            b['page_subcategory'] = 'homepage';
            b['page_type'] = 'homepage';
        }

    /** coporate content */
        else if (path.includes("/chi-siamo")){
            b['website_section'] = 'corporate';
            b['page_category'] = 'about-us';
            b['page_subcategory'] = pathSplit[2];
            b['page_type'] = 'corporate_page';
        }

    /** product catalogue */
        else if (path.includes('prodotti') && jQuery(".product-main-row__description")[3] === undefined){
            b['website_section'] = 'catalogue';
            b['page_category'] = 'product';
            b['page_type'] = 'product_catalogue';
            b['page_subcategory'] = pathSplit[2];
        }

    /** product Page */
        else if (path.includes('prodotti') && jQuery(".product-main-row__description")[3] !== undefined){
            b['website_section'] = 'catalogue';
            b['page_category'] = 'product';
            b['page_type'] = 'product_page';
            b['page_subcategory'] = pathSplit[2];
        }

    /** Recipe Page */
        else if (path.includes('ricette') && jQuery(".recipes__ingredients__title").length){
            b['website_section'] = 'recipes';
            b['page_category'] = 'recipes_page';
            b['page_type'] = 'recipe_page';
            b['page_subcategory'] = pathSplit[2];
        }

    /** Recipe Catalogue */
        else if (path.includes('ricette')){
            b['website_section'] = 'recipes';
            b['page_category'] = 'recipes_list';
            b['page_type'] = 'recipe_list';
            if (pathLEN !== 2){
                b['page_subcategory'] = pathSplit[2];
            }
            else {b['page_subcategory'] = 'all'}
        }

    /** Contact page */
        else if (path.includes('contatti')){
            b['website_section'] = 'corporate';
            b['page_category'] = 'contact_page';
            b['page_type'] = 'corporate_page';
            if (pathLEN !== 2){
                b['page_subcategory'] = pathSplit[2];
            }
            else {b['page_subcategory'] = 'general'}
        }

    /** collect product info for readability */
    b['page_collection'] = new pageCollection(
        b['website_section'],
        b['page_category'],
        b['page_subcategory'],
        b['page_type'],
        b['page_name']
    );
    console.log('site architecture DL has loaded successfully');
    console.log(b['page_collection']);
}
