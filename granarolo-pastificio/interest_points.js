var domain = document.location.protocol+'//'+document.location.hostname;
var path = document.location.pathname;
var url = domain + path;
var b = utag.data;
var urlParams = new URLSearchParams(window.location.search);
var hash = window.location.hash;
var pathSplit = path.split('/') ;
var pathLEN = path.split('/').length;
    b['page_name'] = document.title;


    /** Collection Function  **/
    function productDetails(id, name, brand, category, img, url){
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.category = category;
        this.img = img;
        this.url = url;
    }


if (b["page_type"] !== undefined && b["page_type"] !== "" ){

/** Send Content Page Event */
    if (b["page_type"] === "content_page" ){

        b["event_category"] = 'conversion' ;
        b["event_action"] = 'content_view' ;
        b["event_label"] = b["website_section"];
        b["event_value"] = '';

    }
/** Send Coporate Page Event */
    else if (b["page_type"] === "corporate_page" ){

        b["event_category"] = 'conversion' ;
        b["event_action"] = 'corporate_view' ;
        b["event_label"] = b["page_category"]+'_'+b["page_subcategory"];
        b["event_value"] = '';

    }

/** Product Information */
    else if(b["page_type"] === "product_page"){

        /** custom event **/
        b["tealium_event"] = 'product_page';
    
        b["product_id"] = jQuery(".product-main-row__description")[3].innerText;
        b["product_name"] =  jQuery('.product-title-box__title').text().toLowerCase();
        b["product_image"] = domain + jQuery('img.product-cover-box__cover').attr('src'),
        b["product_brand"] = 'granarolo-pastificio' ;
        b["product_category"] = pathSplit[2] ;
        
        b["product_collection"] = new productDetails(
            b["product_id"],
            b["product_name"],
            b["product_brand"], //product_brand
            b["product_category"], //product_category
            b["product_image"],
            url,
        );

        b["event_category"] = 'conversion' ;
        b["event_action"] = 'product_view' ;
        b["event_label"] = 'product_page';
        b["event_value"] = '';


            console.log('product information DL has successfully loaded ');
            console.log(b["product_collection"])
    }

/** Recipe Information */
    else if(b["page_type"] === "recipe_page"){

        /** custom event **/
        b["tealium_event"] = 'recipe_view';
        b["event_category"] = 'conversion' ;
        b["event_action"] = 'recipe_view' ;
        b["event_label"] = 'recipe_page';
        b["event_value"] = '';

        function recipeCollection(name, category, img, url, details){
            this.name = name;
            this.category = category;
            this.img = img;
            this.url = url;
            this.details = details;
        };

        function recipeDetails(skills, duration, type, serving){
            this.skills = skills;
            this.duration = duration;
            this.cooking_type = type;
            this.serving = serving;
        };

        b["recipe_name"] = jQuery("h1.recipes__info__title").text();
        b["recipe_category"] = pathSplit[2];
        b["recipe_image"] = jQuery('.recipes__cover__image').css('background-image').split('"')[1];
        b["recipe_url"] = url;
        var recipe_info = jQuery('.recipes__info__icon__title').map(function(){return $(this).text()}).get();

        if (jQuery('.recipes__related-product__slug__url').length){
            b["product_id"] = ''
            b["product_name"] = jQuery('.recipes__related-product__title').text();
            b["product_brand"] = 'granarolo-pastificio';
            b["product_category"] = jQuery('.recipes__related-product__slug__url').attr('href').split('/')[2];
            b["product_url"] = domain + jQuery('.recipes__related-product__slug__url').attr('href');
            b["product_image"] = domain + jQuery('.recipes__related-product__image').attr('src');

            b["product_collection"] = new productDetails(
                b["product_id"],
                b["product_name"],
                b["product_brand"], //product_brand
                b["product_category"], //product_category
                b["product_image"],
                url,
            )
        };
        
        b["recipe_details"] = new recipeDetails(
            recipe_info[0],
            recipe_info[1],
            recipe_info[2],
            recipe_info[3]
        );

        b["recipe_collection"] = new recipeCollection(
            b["recipe_name"],
            b["recipe_category"],
            b["recipe_image"], //product_brand
            url,
            b["recipe_details"]
        );

        console.log('recipe information DL has successfully loaded');
        console.log(b["recipe_collection"]);
        console.log(b["product_collection"])

    }

}