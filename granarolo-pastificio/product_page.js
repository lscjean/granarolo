/**
 * product id
 * product name
 * product legal
 * product brand
 * product category
 * product image
 * product ingredient
 * product nutriment
 * product collection
 * 
 * related product collection
 * related Recipe Collection
 * 
 */

var domain = document.location.protocol+'//'+document.location.hostname;
var path = document.location.pathname;
var url = domain + path;
var b = utag.data;
var path = document.location.pathname;
var urlParams = new URLSearchParams(window.location.search);
var hash = window.location.hash;
var pathSplit = path.split('/') ;
var pathLEN = path.split('/').length;
    b['page_name'] = document.title;

if(jQuery(".product-main-row__description")[3].length !== 0){

    /** custom event **/
    b["tealium_event"] = 'product_page';
  
    
    /** Product Information **/
    b["product_id"] = jQuery(".product-main-row__description")[3].innerText;
    b["product_name"] =  jQuery('.product-title-box__title').text().toLowerCase();
    b["product_image"] = domain + jQuery('img.product-cover-box__cover').attr('src'),
    b["product_brand"] = 'granarolo-pastificio' ;
    b["product_category"] = pathSplit[2] ;


    /** Product Collection Function  **/
    function productDetails(id, name, brand, category, img, url){
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.category = category;
        this.img = img;
        this.url = url;
    }
    
    b["product_collection"] = new productDetails(
        b["product_id"],
        b["product_name"],
        b["product_brand"], //product_brand
        b["product_category"], //product_category
        b["product_image"],
        url,
    )


        console.log('product information DL has successfully loaded ')
        console.log(b["product_collection"]);
}
    
