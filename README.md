this document contains the implementation instructions along with the website related to the html file. 

### Library to website ###

tiq-bimbi.html >> granarolo bimbi
tiq-biologico.html >> granarolo biologico
tiq-fresco.html >> granarolo fresco
tiq-gplus.html >> granarolo G+
tiq-gruppo.html >> Gruppo Granarolo
tiq-maestrilatte.html >> Maestri del latte
tiq-main.html >> Granarolo.it
tiq-pastificio.html >> Granarolo Pastificio
tiq-pettinicchio.html >> Granarolo Pettinicchio
tiq-premidaossiduri.html >> Premidaossiduri (deprecicated)

In this section, we will walk you through the implementation of Tealium.
In the code above, you will find a HTML file (.html). This file contain the Tealium Code. 

##### Introduction #####
Tealium is a Data Management Platform (or DMP) which collects information and help marketeer build audiences based on the website traffic. 
In order to do so, Tealium uses an data object called a 'dataLayer' also know as utag.data *(!= utag_data)*. 

Tealium's dataLayer is build upon various information such as: 
* DOM variables, 
* Meta Data Elements, 
* First Party Cookies, 
* Query String Parameters, 
* Javascript Variables, 
* The Universal Data Object (or UDO also known as utag_data). 

##### IMPORTANT #####
* utag_data references the Universal Data Object (UDO), a core element for Tealium
* utag.data references the whole dataLayer including the UDO. 
A perfect illustration of this principal can be found here: https://zjzhh79394.i.lithium.com/t5/image/serverpage/image-id/3263i63773B6C011BEF44/image-size/original?v=1.0&px=-1

For more information about Tealium way of working, please refer to the following urls: 
* https://community.tealiumiq.com/t5/Data-Layer/An-Introduction-to-the-Data-Layer/ta-p/15885
* https://community.tealiumiq.com/t5/iQ-Tag-Management/Data-Layer-Variables/ta-p/9427 

#### Integration with Tealium  ####
In order for Tealium to work correctly, you first need to identify the 3 environments. 
Each of them has its own Tealium script. To know which tag should go where, please refer to the "a" variable in the script: 
* '//tags.tiqcdn.com/utag/granarolo/lib-kickoff/prod/utag.js'
* '//tags.tiqcdn.com/utag/granarolo/lib-kickoff/qa/utag.js'
* '//tags.tiqcdn.com/utag/granarolo/lib-kickoff/dev/utag.js'

Once this is done, the UDO will need to be populated using website variables. 
Among the most important ones for Granarolo are related to product & recipe information.
You will find below the relevant variables and some details attached to it: 

    /** Product Information */
        product_id : "", // Contains product ID(s) - multiple values should be comma-separated.
        product_name : "", // Contains product name
        product_legal : "", // Contains product Legal Name (if it exists)
        product_image : [], // contains the URL of the product's image. This can be an array of urls. 
        product_brand : "", // Contains product brand
        product_category : "", // Contains product category(s) - multiple values should be comma-separated.
        product_subcategory : "", // Contains product subcategory(s) - multiple values should be comma-separated.

    /** Recipe Information */
        recipe_name : "", // Contains the recipe name
        recipe_image : "", // Contains the URL of the Recipe's image
        recipe_category : "", // Contains the category of the recipe 
        recipe_details : {}, // OBJECT containing Skills Duration Healthiness and Serving of the recipe. 

    /** Website Architecture */
        page_type : "", // Contains a user-friendly page type, e.g. 'product_page', 'recipe_page', 'homepage', 'catalogue', etc.
        page_name : "", // Contains a user-friendly name for the page generally document.title

If you see relevant information on your side, feel free to reach out to us and we will set up a call to discuss their integration. 
the information specified above needs to be placed inside the utag_data object, placed above the Tealium Script. 

The Tealium code is designed to be placed after the opening <body> tag (as opposed to the <head> tag). This positioning provides the best compatibility with the greatest number of vendors and allows third-party tracking to complete before the visitor navigates to the next page. The utag.js code can be placed anywhere within the <body>, although the top is the preferred location. The sooner the page can begin loading utag.js the sooner your tags will be ready to run when the page has been rendered.

For more information about the setup, please refer to Tealium Community: 
https://community.tealiumiq.com/t5/JavaScript-utag-js/Adding-utag-js-to-your-site/ta-p/14785 


