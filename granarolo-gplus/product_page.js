var domain = document.location.protocol + document.location.hostname;

/** Product Information */
    b["product_id"] = $('.product-main-row__ean-description-wrapper').children('span')[1].innerText;
    b["product_image"] = domain + $('img.product-cover-box__cover').attr('src'); // as full url
    b["product_brand"] = "granarolo-g-plus";
    b["product_category"] = "latte";
    b["product_url"] = document.location.protocol+'//'+document.location.hostname+document.location.pathname;

    b["product_name"] =  document.querySelector('.product-title-box__title').innerText;
    if (b["product_name.includes('lunga conservazione')"]){
        b["product_subcategory"] = 'lunga conservazione'
    }
    else if (b["product_name.includes('temperatura elevata')"]){
        b["product_subcategory"] = 'temperatura elevata'
    }