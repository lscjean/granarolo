if (document.location.pathname !== undefined){

    var domain = document.location.protocol+'//'+document.location.hostname;
    var path = document.location.pathname;
    var url = domain + path;
    var b = utag.data;
    var path = document.location.pathname;
    var urlParams = new URLSearchParams(window.location.search);
    var hash = window.location.hash;
    var pathSplit = path.split('/') ;
    var pathLEN = path.split('/').length;
        b['page_name'] = document.title

    /** page collection function */

    function pageCollection(website_section, page_category, page_subcategory, page_type, page_name){
        this.section = website_section;
        this.category = page_category;
        this.subcategory = page_subcategory;
        this.type = page_type;
        this.name = page_name;
    }

    /** Corporate */
        if (hash.includes("#cosa-e-granarolo-g-plus")){
            b['website_section'] = 'corporate';
            b['page_category'] = 'about-us';
            b['page_subcategory'] = 'presentation';
            b['page_type'] = 'corporate_page';
        }

    /* homepage */
        else if (path === '/'){
            b['website_section'] = 'homepage';
            b['page_category'] = 'homepage';
            b['page_subcategory'] = 'homepage';
            b['page_type'] = 'homepage';
        }

    /** coporate content */
        else if (path.includes("/come-nasce")){
            b['website_section'] = 'corporate';
            b['page_category'] = 'about-us';
            b['page_subcategory'] = 'origines';
            b['page_type'] = 'corporate_page';
        }

    /** product catalogue */
        else if (path.includes('prodotti') && jQuery('.product-main-row__description')[4] === undefined){
            b['website_section'] = 'catalogue';
            b['page_category'] = 'product';
            b['page_type'] = 'product_catalogue';
            if (path.includes('da-frigo')){
                b['page_subcategory'] = pathSplit[2];
            }
            else {
                b['page_subcategory'] = 'all';
            }
        }

    /** product Page */
        else if (path.includes('prodotti') && jQuery('.product-main-row__description')[4] !== undefined){
            b['website_section'] = 'catalogue';
            b['page_category'] = 'product';
            b['page_type'] = 'product_page';
        }

    /** Content Catalogue */
        else if (path.includes('/un-plus-alla-settimana') && jQuery('.browse-news__filters__container').length !== 0){
            b['website_section'] = 'news';
            b['page_category'] = 'news_list';
            b['page_type'] = 'content_list';
            if (jQuery('.dropdown').text().trim().toLowerCase().includes('tutti')){
                b['page_subcategory'] = 'all';
            }
            else {
                b['page_subcategory'] = jQuery('.dropdown').text().trim().toLowerCase().replace(/ /g,'_');
            }
        }

    /** Content Page */
        else if (path.includes('/un-plus-alla-settimana') && jQuery('.browse-news__filters__container').length === 0){
            b['website_section'] = 'news';
            b['page_category'] = 'news_page';
            b['page_type'] = 'content_page';
            b['page_subcategory'] = jQuery('.press-release__titles').text().trim().split(' - ')[1].slice(9);
        }

    /** corporate page */
        else if (path.includes('un-plus-di-qualita')){
            b['website_section'] = 'corporate';
            b['page_category'] = 'about-us';
            b['page_subcategory'] = 'usp';
            b['page_type'] = 'corporate_page';
        }

    /** collect product info for readability */
    b['page_collection'] = new pageCollection(
        b['website_section'],
        b['page_category'],
        b['page_subcategory'],
        b['page_type'],
        b['page_name']
    );
    console.log('site architecture DL has loaded successfully');
    console.log(b['page_collection']);
}
