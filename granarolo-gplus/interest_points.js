var domain = document.location.protocol+'//'+document.location.hostname;
var path = document.location.pathname;
var url = domain + path;
var b = utag.data

if (b["page_type"] !== undefined && b["page_type"] !== "" ){

/** Send Content Page Event */
    if (b["page_type"] === "content_page" ){

        b["event_category"] = 'conversion' ;
        b["event_action"] = 'content_view' ;
        b["event_label"] = b["website_section"];
        b["event_value"] = '';

    }
/** Send Coporate Page Event */
    else if (b["page_type"] === "corporate_page" ){

        b["event_category"] = 'conversion' ;
        b["event_action"] = 'corporate_view' ;
        b["event_label"] = b["page_category"]+'_'+b["page_subcategory"];
        b["event_value"] = '';

    }

/** Send Product page Event */
    else if (b["page_type"] === "product_page" ){

        b["event_category"] = 'conversion' ;
        b["event_action"] = 'product_view' ;
        b["event_label"] = 'product_page';
        b["event_value"] = '';

        b["product_id"] = $('.product-main-row__ean-description-wrapper').children('span')[1].innerText;
        b["product_name"] =  document.querySelector('.product-title-box__title').innerText;
        b["product_image"] = domain + $('img.product-cover-box__cover').attr('src'); // as full url
        b["product_brand"] = "granarolo-g-plus";
        b["product_category"] = "latte";
        b["product_url"] = document.location.protocol+'//'+document.location.hostname+document.location.pathname;

        if (b["product_name.includes('lunga conservazione')"]){
            b["product_subcategory"] = 'lunga conservazione'
        }
        else if (b["product_name.includes('temperatura elevata')"]){
            b["product_subcategory"] = 'temperatura elevata'
        }

        function productDetails(id, name, brand, category, img, url){
            this.id = id;
            this.name = name;
            this.brand = brand;
            this.category = category;
            this.img = img;
            this.url = url;
        }
        
        b["product_collection"] = new productDetails(
            b["product_id"],
            b["product_name"],
            b["product_brand"],
            b["product_category"], 
            b["product_image"],
            url,
        )

        console.log('product details DL has successfully loaded')
        console.log(b["product_collection"])


    }

}