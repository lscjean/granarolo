var domain = document.location.protocol+'//'+document.location.hostname;
var path = document.location.pathname;
var url = domain + path;
var b = utag.data;
var path = document.location.pathname;
var hash = window.location.hash;
var pathSplit = path.split('/') ;
var pathLEN = path.split('/').length;
    
if (b["page_type"] !== undefined && b["page_type"] !== "" ){

/** Send Content Page Event */
    if (b["page_type"] === "content_page" ){

        b["event_category"] = 'conversion' ;
        b["event_action"] = 'content_view' ;
        b["event_label"] = b["website_section"];
        b["event_value"] = '';

    }
/** Send Coporate Page Event */
    else if (b["page_type"] === "corporate_page" ){

        b["event_category"] = 'conversion' ;
        b["event_action"] = 'corporate_view' ;
        b["event_label"] = b["page_subcategory"];
        b["event_value"] = '';

    }

/** Send Product page Event */
    else if (b["page_type"] === "product_page" ){

        b["product_id"] = jQuery('.product-main-row__description')[4].innerText;
        b["product_image"] = jQuery('img.product__slide__image')[0].src;
        b["product_brand"] = "granarolo-senzaglutine";
        b["product_name"] = document.title;
        b["product_url"] = url;

        b["event_category"] = 'conversion' ;
        b["event_action"] = 'product_view' ;
        b["event_label"] = 'product_page';
        b["event_value"] = '';
    }

/** Send Contact Page Event */
    else if (b["page_type"] === "contact_page" ){

        b["event_category"] = 'conversion' ;
        b["event_action"] = 'contact_view' ;
        b["event_label"] = b["page_category"]+'_'+b["page_subcategory"];
        b["event_value"] = '';

    }
}