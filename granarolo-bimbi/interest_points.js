var b = utag.data
if(b["page_type"] !== undefined) {
/* shortcuts */

    // b["breadcrumb = jQuery('.breadcrumb').textContent.split('\n').map(function(x){return x.trim()}).slice(1);
var domain = document.location.protocol+'//'+document.location.hostname;
var path = document.location.pathname;
var urlParams = new URLSearchParams(window.location.search);
var pathSplit = path.split('/') ;
var pathLenght = path.split('/').length;
var url = domain + path;



    /** Product Pages */
        if (b["page_type"] ===  "product_page" || jQuery('#modalita-conservazione > p:nth-child(10)').length){

        /** Product Information */
        b["product_id"] = jQuery('#modalita-conservazione > p:nth-child(10)').text();
        b["product_name"] = jQuery('.product-page-title').text();
        b["product_image"] = jQuery('.product-page-image > img').attr('src'); // as full url
        b["product_brand"] = 'granarolo-bimbi';
        b["product_category"] = pathSplit[2];
        b["product_subcategory"] = "";


        function productDetails(id, name, brand, category, img, url){
            this.id = id;
            this.name = name;
            this.brand = brand;
            this.category = category;
            this.img = img;
            this.url = url;
        }
        
        b["product_collection"] = new productDetails(
            b["product_id"],
            b["product_name"],
            b["product_brand"], 
            b["product_category"],
            b["product_image"],
            url,
        )

        console.log('product information DL has successfully loaded ');
        console.log(b["product_collection"]);

        /** Send Product Page GA Event */
        b["event_category"] = 'conversion' ;
        b["event_action"] = 'product_view' ;
        b["event_label"] = 'product_page';
        b["event_value"] = '';
    }

    /** Recipe Pages */
        else if (b["page_type"] === "recipe_page" || path.includes('ricette') === true && jQuery('.page-type-title').text() === 'Le Ricette'){
    
        /** recipe information */
        b["recipe_name"] = document.title;
        b["recipe_image"] = jQuery('.img-responsive').attr('src'); // as full url
        b["recipe_category"] = jQuery('.article-category').text(); 
        b["recipe_url"] = url

        function recipeDetails(name, category, img, url){
            this.name = name;
            this.category = category;
            this.img = img;
            this.url = url;
        }
        
        b["recipe_collection"] = new recipeDetails(
            b["recipe_name"],
            b["recipe_category"],
            b["recipe_image"],
            url,
        )

        console.log('Recipe information DL has successfully loaded ')
        console.log(b["recipe_collection"]);

        /** Send Recipe Page GA Event */
        b["event_category"] = 'conversion' ;
        b["event_action"] = 'recipe_view' ;
        b["event_label"] = 'recipe_page';
        b["event_value"] = '';
    }

    /** News Content Pages */
        else if (b["page_type"] === "content_page" && b["page_category"] === "news_page"){
                b["content_category"] = jQuery('.article-category').text();
                b["content_name"] = document.title;
                b["content_img"] = jQuery('.img-responsive').attr('src');
                b["content_url"] = url;

        function contentDetails(name, category, img, url){
            this.name = name;
            this.category = category;
            this.img = img;
            this.url = url;
        }
        
        b["content_collection"] = new contentDetails(
            b["content_name"],
            b["content_category"],
            b["content_img"],
            b["content_url"],
        )

        console.log('Content information DL has successfully loaded ');
        console.log(b["content_collection"]);

        b["event_category"] = 'conversion' ;
        b["event_action"] = 'content_view' ;
        b["event_label"] = 'news';
        b["event_value"] = '';
    }

    /** Experts Content Pages */
    else if (b["page_type"] === "content_page" && b["page_category"] === "tips_page"){
        b["content_category"] = b["page_subcategory"];
        b["content_name"] = document.title;
        b["content_url"] = url;

        function tipsDetails(name, category, url){
            this.name = name;
            this.category = category;
            this.url = url;
        }
        
        b["tips_collection"] = new tipsDetails(
            b["content_name"],
            b["content_category"],
            b["content_url"],
        )

        console.log('Content information DL has successfully loaded ');
        console.log(b["tips_collection"]);

        b["event_category"] = 'conversion' ;
        b["event_action"] = 'content_view' ;
        b["event_label"] = 'experts';
        b["event_value"] = '';
    }

    /** Coporate Pages */
    else if(b["page_type"] === 'corporate_page'){
        b["event_category"] = 'conversion' ;
        b["event_action"] = 'corporate_view' ;
        b["event_label"] = 'corporate_page';
        b["event_value"] = '';

    console.log('Coporate DL has successfully loaded ');

    }

    
console.log('Interest Points DL has successfully loaded ');
console.log(b["event_action"])
}


