
/* shortcuts */

    // utag.data.breadcrumb = $('.breadcrumb').textContent.split('\n').map(function(x){return x.trim()}).slice(1);
    var domain = document.location.protocol+'//'+document.location.hostname;
    var path = document.location.pathname;
    var urlParams = new URLSearchParams(window.location.search);
    var pathSplit = path.split('/') ;
    var pathLenght = path.split('/').length;
    var url = domain + path;


/** Product Pages */
if (utag.data.page_type ===  "product_page" || $('#modalita-conservazione > p:nth-child(10)') !== null ){

    /** Product Information */
    utag.data.product_id = $('#modalita-conservazione > p:nth-child(10)').textContent;
    utag.data.product_name = $('.product-page-title').textContent;
    utag.data.product_image = $('.product-page-image > img').src; // as full url
    utag.data.product_brand = 'granarolo-bimbi';
    utag.data.product_category = pathSplit[2];
    utag.data.product_subcategory = "";

    /** Related Product Information */
    utag.data.relatedProduct_name = [];
    utag.data.relatedProduct_img = []; // as full url
    utag.data.relatedProduct_url = [];
    utag.data.relatedProduct_brand = [];
    utag.data.relatedProduct_category = [];

    function productDetails(id, name, brand, category, img, url){
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.category = category;
        this.img = img;
        this.url = url;
    }
    
    utag.data.product_collection = new productDetails(
        utag.data.product_id,
        utag.data.product_name,
        utag.data.product_brand, 
        utag.data.product_category,
        utag.data.product_image,
        url,
    )

    console.log('product information are loaded ')
    console.log(utag.data.product_collection);

}