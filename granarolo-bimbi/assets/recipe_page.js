utag.data.recipe_name = document.title;
utag.data.recipe_image = $('.img-responsive').src; // as full url
utag.data.recipe_category = $('.article-category').text; 
utag.data.website_section = "recipes";
utag.data.page_category = "recipe_page";
utag.data.page_subcategory = "";
utag.data.page_type = "recipe_page"; // i.e. product_page; recipe_page; homepage; catalogue; etc.
utag.data.page_name = "recipe - " + utag.data.recipe_name ;
