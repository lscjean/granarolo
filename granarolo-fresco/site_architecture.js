if (document.location.pathname !== undefined){

    /* shortcuts */
    
    var domain = document.location.protocol+'//'+document.location.hostname;
    var hostname = document.location.hostname;
    var path = document.location.pathname;
    var url = domain + path;
    var b = utag.data;
    var path = document.location.pathname;
    var urlParams = new URLSearchParams(window.location.search);
    var pathSplit = path.split('/') ;
    var pathLenght = path.split('/').length;
        b['page_name'] = document.title;

    
    /** page collection function */
    function pageCollection(website_section, page_category, page_subcategory, page_type, page_name){
        this.section = website_section;
        this.category = page_category;
        this.subcategory = page_subcategory;
        this.type = page_type;
        this.name = page_name;
    }
    
/* homepage */
    if (path === '/' && hostname==="www.granarolofresco.it"){
        b['website_section'] = 'main_homepage';
        b['page_category'] = 'homepage';
        b['page_subcategory'] = 'homepage';
        b['page_type'] = 'homepage';
    }

/* product catalogue */
    else if (path.includes('prodotti') === true){
        b['website_section'] = 'catalogue';
        b['page_category'] = 'products';
        b['page_type'] = b['page_category'] +'_'+ b['website_section'];
    }

/** Corporate Content Page */
    else if(path.includes('/iniziative')){
        b['website_section'] = 'corporate';
        b['page_category'] = 'iniziative';
        b['page_type'] = 'corporate_page';
    }

    else if(path.includes('/filiera')){
        b['website_section'] = 'corporate';
        b['page_category'] = 'filiera';
        b['page_subcategory'] = pathSplit[1].split('-')[1]
        b['page_type'] = 'corporate_page';
    }

/** Content Section */
    else if (hostname.includes('storie')){
        b['website_section'] = 'stories';
        b['page_category'] = 'stories_pages';
        b['page_type'] = 'content_section';
    }


/** collect product info for readability */
    b['page_collection'] = new pageCollection(
        b['website_section'],
        b['page_category'],
        b['page_subcategory'],
        b['page_type'],
        b['page_name']
    );

console.log('site architecture DL has loaded successfully');
console.log(b['page_collection']);
}
    