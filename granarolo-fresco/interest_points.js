var b = utag.data
if (b["page_type"] !== undefined && b["page_type"] !== "" ){

/** Send Content Page Event */
    if (b["page_type"] === "content_section" ){

        b["event_category"] = 'conversion' ;
        b["event_action"] = 'content_view' ;
        b["event_label"] = b["website_section"];
        b["event_value"] = '';

    }
/** Send Coporate Page Event */
    else if (b["page_type"] === "corporate_page" ){

        b["event_category"] = 'conversion' ;
        b["event_action"] = 'corporate_view' ;
        b["event_label"] = b["page_category"]+'_'+b["page_subcategory"];
        b["event_value"] = '';

    }
}